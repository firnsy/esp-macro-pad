#ifndef __ESP_STORAGE__
#define __ESP_STORAGE__

#include <esp_err.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Intialise ESP Storage
 *
 * @return
 *     - ESP_FAIL
 *     - ESP_OK
 */
esp_err_t esp_storage_init(void);

#ifdef __cplusplus
}
#endif

#endif // __ESP_STORAGE__
