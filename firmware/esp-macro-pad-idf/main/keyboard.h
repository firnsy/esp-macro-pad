#ifndef __KEYBOARD_H__
#define __KEYBOARD_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef enum keyevent_type_t {
  TICK_EVENT = 0,
  KEY_EVENT = 1
} keyevent_type_t;

/* key event */
typedef struct {
    uint8_t         key;       // index of key
    uint32_t        time;
    keyevent_type_t type;
    bool            pressed;
} keyevent_t;

typedef struct {
  uint32_t key_state;    // bitmap of pressed keys
  uint32_t time_ref;     // time keystate was read
} keystate_t;

//#define MAKE_EVENT(key, press, event_type) ((keyevent_t){.key = MAKE_KEYPOS((row_num), (col_num)), .pressed = (press), .time = (), .type = (event_type)})

extern uint16_t keystate_local;
extern uint16_t keystate_remote;

#ifdef __cplusplus
}
#endif

#endif // __KEYBOARD_H__
