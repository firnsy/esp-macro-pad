#include "espnow.h"

#define ESPNOW_MAXDELAY 512

static const char *TAG2 = "espnow_example";

static QueueHandle_t s_emk_espnow_queue;

static uint8_t s_emk_broadcast_mac[ESP_NOW_ETH_ALEN] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

static uint16_t s_emk_espnow_seq[EMK_ESPNOW_DATA_MAX] = { 0, 0, 0 };


/* WiFi should start before using ESPNOW */
void emk_wifi_init(void) {
  ESP_ERROR_CHECK(esp_netif_init());
  ESP_ERROR_CHECK(esp_event_loop_create_default());
  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  ESP_ERROR_CHECK(esp_wifi_init(&cfg));
  ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
  ESP_ERROR_CHECK(esp_wifi_set_mode(ESPNOW_WIFI_MODE));
  ESP_ERROR_CHECK(esp_wifi_start());
  ESP_ERROR_CHECK(esp_wifi_set_channel(CONFIG_ESPNOW_CHANNEL, WIFI_SECOND_CHAN_NONE));

#if CONFIG_ESPNOW_ENABLE_LONG_RANGE
  ESP_ERROR_CHECK(esp_wifi_set_protocol(ESPNOW_WIFI_IF, WIFI_PROTOCOL_11B|WIFI_PROTOCOL_11G|WIFI_PROTOCOL_11N|WIFI_PROTOCOL_LR));
#endif
}

/* ESPNOW sending or receiving callback function is called in WiFi task.
 * Users should not do lengthy operations from this task. Instead, post
 * necessary data to a queue and handle it from a lower priority task. */
static void emk_espnow_send_cb(const uint8_t *mac_addr, esp_now_send_status_t status) {
  emk_espnow_event_t evt;
  emk_espnow_event_send_cb_t *send_cb = &evt.info.send_cb;

  if (mac_addr == NULL) {
    ESP_LOGE(TAG2, "Send cb arg error");
    return;
  }

  evt.id = EMK_ESPNOW_SEND_CB;
  memcpy(send_cb->mac_addr, mac_addr, ESP_NOW_ETH_ALEN);
  send_cb->status = status;

  if (xQueueSend(s_emk_espnow_queue, &evt, ESPNOW_MAXDELAY) != pdTRUE) {
    ESP_LOGW(TAG2, "Send send queue fail");
  }
}

static void emk_espnow_recv_cb(const esp_now_recv_info_t *recv_info, const uint8_t *data, int len) {
  emk_espnow_event_t evt;
  emk_espnow_event_recv_cb_t *recv_cb = &evt.info.recv_cb;
  uint8_t * mac_addr = recv_info->src_addr;

  if (mac_addr == NULL || data == NULL || len <= 0) {
    ESP_LOGE(TAG2, "Receive cb arg error");
    return;
  }

  evt.id = EMK_ESPNOW_RECV_CB;
  memcpy(recv_cb->mac_addr, mac_addr, ESP_NOW_ETH_ALEN);
  recv_cb->data = (uint8_t *)malloc(len);
  if (recv_cb->data == NULL) {
    ESP_LOGE(TAG2, "Malloc receive data fail");
    return;
  }

  memcpy(recv_cb->data, data, len);
  recv_cb->data_len = len;
  if (xQueueSend(s_emk_espnow_queue, &evt, ESPNOW_MAXDELAY) != pdTRUE) {
    ESP_LOGW(TAG2, "Send receive queue fail");
    free(recv_cb->data);
  }
}

/* Parse received ESPNOW data. */
int emk_espnow_data_parse(uint8_t *data, uint16_t data_len, uint8_t *state, uint16_t *seq, int *magic, uint16_t *keystate) {
  emk_espnow_data_t *buf = (emk_espnow_data_t *)data;
  uint16_t crc, crc_cal = 0;

  if (data_len < sizeof(emk_espnow_data_t)) {
    ESP_LOGE(TAG2, "Receive ESPNOW data too short, len:%d", data_len);
    return -1;
  }

  *state = buf->state;
  *seq = buf->seq_num;
  *magic = buf->magic;
  *keystate = (buf->payload[1] << 8) | buf->payload[0];

  crc = buf->crc;
  buf->crc = 0;
  crc_cal = esp_crc16_le(UINT16_MAX, (uint8_t const *)buf, data_len);

  if (crc_cal == crc) {
    return buf->type;
  }

  return -1;
}

void emk_espnow_send_keystate(keystate_t keystate) {
  emk_espnow_data_t data;

  const uint8_t *data_ref = (uint8_t const *)&data;
  size_t data_len = sizeof(data);

  ESP_LOGI(TAG2, "KS: %04X (%ld)\n", (uint16_t)keystate.key_state, keystate.time_ref);

  data.type = EMK_ESPNOW_DATA_KEYBOARD;
  data.state = 0;
  data.seq_num = 0;
  data.magic = 0;
  data.crc = 0;

  data.payload[0] = keystate.key_state & 0xff;
  data.payload[1] = (keystate.key_state >> 8) & 0xff;

  data.crc = esp_crc16_le(UINT16_MAX, data_ref, data_len);

  // broadcast keystate
  if (esp_now_send(s_emk_broadcast_mac, data_ref, data_len) != ESP_OK) {
    ESP_LOGE(TAG2, "Send error");
  }
}

/* prepare espnow data to be sent. */
void emk_espnow_data_prepare(emk_espnow_send_param_t *send_param) {
  emk_espnow_data_t *buf = (emk_espnow_data_t *)send_param->buffer;

  assert(send_param->len >= sizeof(emk_espnow_data_t));

  buf->type = IS_BROADCAST_ADDR(send_param->dest_mac) ?
    EMK_ESPNOW_DATA_BROADCAST :
    EMK_ESPNOW_DATA_UNICAST;

  buf->state = send_param->state;
  buf->seq_num = s_emk_espnow_seq[buf->type]++;
  buf->crc = 0;
  buf->magic = send_param->magic;

  /* fill all remaining bytes after the data with random values */
  buf->payload[0] = 31;
  buf->payload[1] = 41;

  buf->crc = esp_crc16_le(UINT16_MAX, (uint8_t const *)buf, send_param->len);
}

static void emk_espnow_task(void *pvParameter) {
  emk_espnow_event_t evt;
  uint8_t recv_state = 0;
  uint16_t recv_seq = 0;
  uint16_t recv_keystate;

  int recv_magic = 0;
  bool is_broadcast = false;
  int ret;

  vTaskDelay(5000 / portTICK_PERIOD_MS);
  ESP_LOGI(TAG2, "Start sending broadcast data");

  /* Start sending broadcast ESPNOW data. */
  emk_espnow_send_param_t *send_param = (emk_espnow_send_param_t *)pvParameter;
  if (esp_now_send(send_param->dest_mac, send_param->buffer, send_param->len) != ESP_OK) {
    ESP_LOGE(TAG2, "Send error");
    emk_espnow_deinit(send_param);
    vTaskDelete(NULL);
  }

  while (xQueueReceive(s_emk_espnow_queue, &evt, portMAX_DELAY) == pdTRUE) {
    switch (evt.id) {
      case EMK_ESPNOW_SEND_CB:
        {
          emk_espnow_event_send_cb_t *send_cb = &evt.info.send_cb;
          is_broadcast = IS_BROADCAST_ADDR(send_cb->mac_addr);

          ESP_LOGD(TAG2, "Send data to "MACSTR", status1: %d", MAC2STR(send_cb->mac_addr), send_cb->status);

          break;
        }
      case EMK_ESPNOW_RECV_CB:
        {
          emk_espnow_event_recv_cb_t *recv_cb = &evt.info.recv_cb;

          ret = emk_espnow_data_parse(recv_cb->data, recv_cb->data_len, &recv_state, &recv_seq, &recv_magic, &recv_keystate);
          free(recv_cb->data);

          if (ret == EMK_ESPNOW_DATA_BROADCAST) {
            ESP_LOGI(TAG2, "Receive %dth broadcast data from: "MACSTR", len: %d", recv_seq, MAC2STR(recv_cb->mac_addr), recv_cb->data_len);

            /* if MAC address does not exist in peer list, add it to peer list. */
            if (esp_now_is_peer_exist(recv_cb->mac_addr) == false) {
              esp_now_peer_info_t *peer = (esp_now_peer_info_t *)malloc(sizeof(esp_now_peer_info_t));
              if (peer == NULL) {
                ESP_LOGE(TAG2, "Malloc peer information fail");
                emk_espnow_deinit(send_param);
                vTaskDelete(NULL);
              }

              // clear peer info and populate
              memset(peer, 0, sizeof(esp_now_peer_info_t));
              peer->channel = CONFIG_ESPNOW_CHANNEL;
              peer->ifidx = ESPNOW_WIFI_IF;
              peer->encrypt = true;
              memcpy(peer->lmk, CONFIG_ESPNOW_LMK, ESP_NOW_KEY_LEN);
              memcpy(peer->peer_addr, recv_cb->mac_addr, ESP_NOW_ETH_ALEN);
              ESP_ERROR_CHECK(esp_now_add_peer(peer));
              free(peer);
            }

            /* indicates that the device has received broadcast ESPNOW data. */
            if (send_param->state == 0) {
              send_param->state = 1;
            }

            /* if receive broadcast ESPNOW data which indicates that the other device has received
             * broadcast ESPNOW data and the local magic number is bigger than that in the received
             * broadcast ESPNOW data, stop sending broadcast ESPNOW data and start sending unicast
             * ESPNOW data.
             */
            if (recv_state == 1) {
              /* The device which has the bigger magic number sends ESPNOW data, the other one
               * receives ESPNOW data.
               */
              if (send_param->unicast == false && send_param->magic >= recv_magic) {
                ESP_LOGI(TAG2, "Start sending unicast data");
                ESP_LOGI(TAG2, "send data to "MACSTR"", MAC2STR(recv_cb->mac_addr));

                /* Start sending unicast ESPNOW data. */
                memcpy(send_param->dest_mac, recv_cb->mac_addr, ESP_NOW_ETH_ALEN);
                emk_espnow_data_prepare(send_param);
                if (esp_now_send(send_param->dest_mac, send_param->buffer, send_param->len) != ESP_OK) {
                  ESP_LOGE(TAG2, "Send error");
                  emk_espnow_deinit(send_param);
                  vTaskDelete(NULL);
                } else {
                  send_param->broadcast = false;
                  send_param->unicast = true;
                }
              }
            }
          } else if (ret == EMK_ESPNOW_DATA_UNICAST) {
            ESP_LOGI(TAG2, "Receive %dth unicast data from: "MACSTR", len: %d", recv_seq, MAC2STR(recv_cb->mac_addr), recv_cb->data_len);

            /* If receive unicast ESPNOW data, also stop sending broadcast ESPNOW data. */
            send_param->broadcast = false;
          } else if (ret == EMK_ESPNOW_DATA_KEYBOARD) {
            ESP_LOGI(TAG2, "Received broadcasted keyboard state from: "MACSTR", %02X", MAC2STR(recv_cb->mac_addr), recv_keystate);

            keystate_remote = recv_keystate;
          } else {
            ESP_LOGI(TAG2, "Receive error data from: "MACSTR"", MAC2STR(recv_cb->mac_addr));
          }
          break;
        }
      default:
        ESP_LOGE(TAG2, "Callback type error: %d", evt.id);
        break;
    }
  }
}

esp_err_t emk_espnow_init(uint8_t device_index) {
  emk_espnow_send_param_t *send_param;

  s_emk_espnow_queue = xQueueCreate(ESPNOW_QUEUE_SIZE, sizeof(emk_espnow_event_t));
  if (s_emk_espnow_queue == NULL) {
    ESP_LOGE(TAG2, "Create mutex fail");
    return ESP_FAIL;
  }

  /* Initialize ESPNOW and register sending and receiving callback function. */
  ESP_ERROR_CHECK(esp_now_init() );
  ESP_ERROR_CHECK(esp_now_register_send_cb(emk_espnow_send_cb));
  ESP_ERROR_CHECK(esp_now_register_recv_cb(emk_espnow_recv_cb));

#if CONFIG_ESPNOW_ENABLE_POWER_SAVE
  ESP_ERROR_CHECK(esp_now_set_wake_window(CONFIG_ESPNOW_WAKE_WINDOW));
  ESP_ERROR_CHECK(esp_wifi_connectionless_module_set_wake_interval(CONFIG_ESPNOW_WAKE_INTERVAL));
#endif

  /* set primary master key. */
  ESP_ERROR_CHECK(esp_now_set_pmk((uint8_t *)CONFIG_ESPNOW_PMK));

  /* add broadcast peer information to peer list. */
  esp_now_peer_info_t *peer = (esp_now_peer_info_t *)malloc(sizeof(esp_now_peer_info_t));
  if (peer == NULL) {
    ESP_LOGE(TAG2, "Malloc peer information fail");
    vSemaphoreDelete(s_emk_espnow_queue);
    esp_now_deinit();
    return ESP_FAIL;
  }

  memset(peer, 0, sizeof(esp_now_peer_info_t));
  peer->channel = CONFIG_ESPNOW_CHANNEL;
  peer->ifidx = ESPNOW_WIFI_IF;
  peer->encrypt = false;
  memcpy(peer->peer_addr, s_emk_broadcast_mac, ESP_NOW_ETH_ALEN);
  ESP_ERROR_CHECK( esp_now_add_peer(peer) );
  free(peer);

  /* initialize sending parameters. */
  send_param = (emk_espnow_send_param_t *) malloc(sizeof(emk_espnow_send_param_t));
  if (send_param == NULL) {
    ESP_LOGE(TAG2, "Malloc send parameter fail");
    vSemaphoreDelete(s_emk_espnow_queue);
    esp_now_deinit();
    return ESP_FAIL;
  }

  memset(send_param, 0, sizeof(emk_espnow_send_param_t));
  send_param->unicast = false;
  send_param->broadcast = true;
  send_param->state = 0;
  send_param->magic = esp_random();
  send_param->count = CONFIG_ESPNOW_SEND_COUNT;
  send_param->delay = CONFIG_ESPNOW_SEND_DELAY;

  send_param->len = CONFIG_ESPNOW_SEND_LEN;
  send_param->buffer = (uint8_t *)malloc(CONFIG_ESPNOW_SEND_LEN);

  if (send_param->buffer == NULL) {
    ESP_LOGE(TAG2, "Malloc send buffer fail");
    free(send_param);
    vSemaphoreDelete(s_emk_espnow_queue);
    esp_now_deinit();
    return ESP_FAIL;
  }
  memcpy(send_param->dest_mac, s_emk_broadcast_mac, ESP_NOW_ETH_ALEN);
  emk_espnow_data_prepare(send_param);

  xTaskCreate(emk_espnow_task, "emk_espnow_task", 4096, send_param, 4, NULL);

  return ESP_OK;
}

void emk_espnow_deinit(emk_espnow_send_param_t *send_param) {
  free(send_param->buffer);
  free(send_param);
  vSemaphoreDelete(s_emk_espnow_queue);
  esp_now_deinit();
}
