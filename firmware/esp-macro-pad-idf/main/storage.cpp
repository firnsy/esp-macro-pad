#include "storage.hpp"

#include "esp_log.h"

#include "nvs.h"
#include "nvs_flash.h"

static const char *TAG = "esp-storage";

esp_err_t esp_storage_init() {
  static bool init_flag = false;

  if (!init_flag) {
    esp_err_t ret = nvs_flash_init();

    if (
      ret == ESP_ERR_NVS_NO_FREE_PAGES ||
      ret == ESP_ERR_NVS_NEW_VERSION_FOUND
    ) {
      // NVS partition was truncated and needs to be erased
      // retry nvs_flash_init
      ESP_ERROR_CHECK( nvs_flash_erase() );
      ret = nvs_flash_init();
    }

    ESP_ERROR_CHECK(ret);

    init_flag = true;
  }

  return ESP_OK;
}
