#ifndef __EMP_LCD_HPP__
#define __EMP_LCD_HPP__

#define LGFX_WT32_SC01
#define LGFX_USE_V1

#include <LovyanGFX.hpp>
#include <lgfx/v1/panel/Panel_LCD.hpp>
#include <lgfx/v1/panel/Panel_ST7735.hpp>

namespace lgfx {
  inline namespace v1 {
    struct Panel_ST7735GT : public Panel_ST7735 {
      protected:
        static constexpr uint8_t CMD_INVOFF  = 0x20;
        static constexpr uint8_t CMD_MADCTL  = 0x36;
        static constexpr uint8_t CMD_COLMOD  = 0x3A;

        const uint8_t* getInitCommands(uint8_t listno) const override {
          static constexpr uint8_t Rcmd1[] = {                 // Init for 7735R, part 1 (red or green tab)
            CMD_SWRESET, CMD_INIT_DELAY,  //  1: Software reset, 0 args, w/delay
            150,                    //     150 ms delay
            CMD_SLPOUT , CMD_INIT_DELAY,  //  2: Out of sleep mode, 0 args, w/delay
            255,                  //     500 ms delay
            CMD_FRMCTR1, 3      , //  3: Frame rate ctrl - normal mode, 3 args:
            0x01, 0x2C, 0x2D,     //     Rate = fosc/(1x2+40) * (LINE+2C+2D)
            CMD_FRMCTR2, 3      , //  4: Frame rate control - idle mode, 3 args:
            0x01, 0x2C, 0x2D,     //     Rate = fosc/(1x2+40) * (LINE+2C+2D)
            CMD_FRMCTR3, 6      , //  5: Frame rate ctrl - partial mode, 6 args:
            0x01, 0x2C, 0x2D,     //     Dot inversion mode
            0x01, 0x2C, 0x2D,     //     Line inversion mode
            CMD_INVCTR , 1      , //  6: Display inversion ctrl, 1 arg, no delay:
            0x07,                 //     No inversion
            CMD_PWCTR1 , 3      , //  7: Power control, 3 args, no delay:
            0xA2,
            0x02,                 //     -4.6V
            0x84,                 //     AUTO mode
            CMD_PWCTR2 , 1      , //  8: Power control, 1 arg, no delay:
            0xC5,                 //     VGH25 = 2.4C VGSEL = -10 VGH = 3 * AVDD
            CMD_PWCTR3 , 2      , //  9: Power control, 2 args, no delay:
            0x0A,                 //     Opamp current small
            0x00,                 //     Boost frequency
            CMD_PWCTR4 , 2      , // 10: Power control, 2 args, no delay:
            0x8A,                 //     BCLK/2, Opamp current small & Medium low
            0x2A,
            CMD_PWCTR5 , 2      , // 11: Power control, 2 args, no delay:
            0x8A, 0xEE,
            CMD_VMCTR1 , 1      , // 12: Power control, 1 arg, no delay:
            0x0E,
            CMD_INVOFF , 0   ,    // 13: Don't invert display, no args, no delay
            CMD_MADCTL , 1   ,    // 14: Memory access control (directions), 1 arg:
            0xC0 | 0x00 ,         //     row addr/col addr, bottom to top refresh
            CMD_COLMOD , 1   ,    // 15: set color mode, 1 arg, no delay:
            0x05,                 //     16-bit color
            0xFF, 0xFF
          };
          static constexpr uint8_t Rcmd2[] = {  // Init for 7735R, part 2 (red or green tab)
            CMD_GMCTRP1, 16      , //  1: 16 args, no delay:
            0x02, 0x1c, 0x07, 0x12,
            0x37, 0x32, 0x29, 0x2d,
            0x29, 0x25, 0x2B, 0x39,
            0x00, 0x01, 0x03, 0x10,
            CMD_GMCTRN1, 16      , //  2: 16 args, no delay:
            0x03, 0x1d, 0x07, 0x06,
            0x2E, 0x2C, 0x29, 0x2D,
            0x2E, 0x2E, 0x37, 0x3F,
            0x00, 0x00, 0x02, 0x10,
            CMD_NORON  ,    CMD_INIT_DELAY, //  3: Normal display on, no args, w/delay
            10,                     //     10 ms delay
            CMD_DISPON ,    CMD_INIT_DELAY, //  4: Main screen turn on, no args w/delay
            100,                    //     100 ms delay
            0xFF,0xFF
          };

          switch (listno) {
            case 0: return Rcmd1;
            case 1: return Rcmd2;
            default: return nullptr;
          }
        }

        uint8_t getMadCtl(uint8_t r) const override {
          static constexpr uint8_t madctl_table[] = {
            MAD_MY | MAD_MX,
            MAD_MY | MAD_MV,
            0,
            MAD_MX | MAD_MV,
            MAD_MX,
            MAD_MV,
            MAD_MY | 0,
            MAD_MY | MAD_MX | MAD_MV,
          };

          return madctl_table[r];
        }

        void setRotation(uint_fast8_t r) {
          r &= 7;
          _rotation = r;
          _internal_rotation = ((r + _cfg.offset_rotation) & 3) | ((r & 4) ^ (_cfg.offset_rotation & 4));

          auto ox = _cfg.offset_x;
          auto oy = _cfg.offset_y;
          auto pw = _cfg.panel_width;
          auto ph = _cfg.panel_height;
          auto mw = _cfg.memory_width;
          auto mh = _cfg.memory_height;

          if (_internal_rotation & 1) {
            std::swap(ox, oy);
            std::swap(pw, ph);
            std::swap(mw, mh);
          }

          _width  = pw;
          _height = ph;
          _colstart = ox;

          _rowstart = oy;

          _xs = _xe = _ys = _ye = INT16_MAX;

          update_madctl();
        }
    };
  }
}
// custom LCD device setup
class LCD : public lgfx::LGFX_Device {
  lgfx::Panel_ST7735GT _panel_instance;
  lgfx::Bus_SPI _bus_instance;

  public:
    LCD(void) {
      {
        auto cfg = _bus_instance.config();
        cfg.pin_sclk = 12;
        cfg.pin_mosi = 11;
        cfg.pin_miso = -1;
        cfg.pin_dc   = 18;
        _bus_instance.config(cfg);
        _panel_instance.setBus(&_bus_instance);
      }

      {
        auto cfg = _panel_instance.config();
        cfg.pin_cs           = 17;
        cfg.pin_rst          = 3;
        cfg.pin_busy         = -1;
        cfg.memory_width     = 80;
        cfg.memory_height    = 160;
        cfg.panel_width      = 80;
        cfg.panel_height     = 160;
        cfg.offset_x         = 26;
        cfg.offset_y         = 1;
        cfg.offset_rotation  = 3;
        cfg.invert           = true;
        cfg.rgb_order        = false;
        cfg.dlen_16bit       = false;
        cfg.bus_shared       = false;

        _panel_instance.config(cfg);
      }

      setPanel(&_panel_instance);
    }
};

#endif
