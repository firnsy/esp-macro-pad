#ifndef __ESPNOW_H__
#define __ESPNOW_H__

/* ESPNOW can work in both station and softap mode. It is configured in menuconfig. */
#if CONFIG_ESPNOW_WIFI_MODE_STATION
#define ESPNOW_WIFI_MODE WIFI_MODE_STA
#define ESPNOW_WIFI_IF   ESP_IF_WIFI_STA
#else
#define ESPNOW_WIFI_MODE WIFI_MODE_AP
#define ESPNOW_WIFI_IF   WIFI_IF_AP
#endif

#define ESPNOW_QUEUE_SIZE           6

#define IS_BROADCAST_ADDR(addr) (memcmp(addr, s_emk_broadcast_mac, ESP_NOW_ETH_ALEN) == 0)

#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <assert.h>
#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "freertos/timers.h"
#include "nvs_flash.h"
#include "esp_random.h"
#include "esp_event.h"
#include "esp_netif.h"
#include "esp_wifi.h"
#include "esp_log.h"
#include "esp_mac.h"
#include "esp_now.h"
#include "esp_crc.h"

#include "keyboard.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
  EMK_ESPNOW_SEND_CB,
  EMK_ESPNOW_RECV_CB,
} emk_espnow_event_id_t;

typedef struct {
  uint8_t               mac_addr[ESP_NOW_ETH_ALEN];
  esp_now_send_status_t status;
} emk_espnow_event_send_cb_t;

typedef struct {
  uint8_t mac_addr[ESP_NOW_ETH_ALEN];
  uint8_t *data;
  int     data_len;
} emk_espnow_event_recv_cb_t;

typedef union {
  emk_espnow_event_send_cb_t send_cb;
  emk_espnow_event_recv_cb_t recv_cb;
} emk_espnow_event_info_t;

/* when espnow sending or receiving callback function is called, post event to espnow task. */
typedef struct {
    emk_espnow_event_id_t   id;
    emk_espnow_event_info_t info;
} emk_espnow_event_t;

enum {
    EMK_ESPNOW_DATA_BROADCAST,
    EMK_ESPNOW_DATA_UNICAST,
    EMK_ESPNOW_DATA_KEYBOARD,
    EMK_ESPNOW_DATA_MAX,
};

/* user defined field of espnow data in this example. */
typedef struct {
  uint8_t   type;                // broadcast or unicast espnow data.
  uint8_t   state;               // indicate that if has received broadcast espnow data or not.
  uint16_t  seq_num;             // sequence number of espnow data.
  uint16_t  crc;                 // crc16 value of espnow data.
  uint32_t  magic;               // magic number which is used to determine which device to send unicast espnow data.
  uint8_t   payload[8];          // real payload of espnow data.
} __attribute__((packed)) emk_espnow_data_t;

/* parameters of sending espnow data. */
typedef struct {
  bool      unicast;                     // send unicast espnow data.
  bool      broadcast;                   // send broadcast espnow data.
  uint8_t   state;                       // indicate that if has received broadcast espnow data or not.
  uint32_t  magic;                       // magic number which is used to determine which device to send unicast espnow data.
  uint16_t  count;                       // total count of unicast espnow data to be sent.
  uint16_t  delay;                       // delay between sending two espnow data, unit: ms.
  int       len;                         // length of espnow data to be sent, unit: byte.
  uint8_t   *buffer;                     // buffer pointing to espnow data.
  uint8_t   dest_mac[ESP_NOW_ETH_ALEN];  // mac address of destination device.
} emk_espnow_send_param_t;


void emk_wifi_init(void);
esp_err_t emk_espnow_init(uint8_t device_index);
void emk_espnow_deinit(emk_espnow_send_param_t *send_param);

void emk_espnow_send_keystate(keystate_t keystate);

#ifdef __cplusplus
}
#endif

#endif // __ESPNOW_H__
