## Architecture

Main Task
 - Read local keystate
 - Process keystate

 - If keystate change:
     - Broadcast
     - Render keystate

Wireless Comms Task
 - Read events received

 - If keystate event:
    Render remote keystate
