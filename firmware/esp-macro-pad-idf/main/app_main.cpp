#include <stdio.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "esp_chip_info.h"
#include "esp_log.h"
#include "esp_mac.h"
#include "esp_now.h"
#include "esp_timer.h"
#include "esp_wifi.h"

#include "driver/i2c.h"
#include "driver/gpio.h"

#include "espnow.h"
#include "lcd.hpp"
#include "storage.hpp"
#include "keyboard.h"

static const char *TAG = "esp-macro-pad";

/* i2c */
#define I2C_MASTER_NUM              0             // I2C master i2c port number, the number of i2c peripheral interfaces available will depend on the chip
#define I2C_MASTER_TX_BUF_DISABLE   0             // I2C master doesn't need buffer
#define I2C_MASTER_RX_BUF_DISABLE   0             // I2C master doesn't need buffer
#define I2C_MASTER_TIMEOUT_MS       1000
#define I2C_MASTER_FREQ_HZ          400000

// TCA9555 registers
#define TCA9555_SENSOR_ADDR               0x20
#define TCA9555_INPUT_PORT_REGISTER_0     0x00    // read
#define TCA9555_INPUT_PORT_REGISTER_1     0x01
#define TCA9555_OUTPUT_PORT_REGISTER_0    0x02    // write
#define TCA9555_OUTPUT_PORT_REGISTER_1    0x03
#define TCA9555_POLARITY_REGISTER_0       0x04    // get/setPolarity
#define TCA9555_POLARITY_REGISTER_1       0x05
#define TCA9555_CONFIGURATION_PORT_0      0x06    // pinMode
#define TCA9555_CONFIGURATION_PORT_1      0x07
/* /i2c - end */


#define PAD_PIN_I2C_MASTER_SCL 9
#define PAD_PIN_I2C_MASTER_SDA 8

#define PAD_PIN_IO_EXPANDER_INT 36

#define PAD_PIN_LCD_SCLK            12
#define PAD_PIN_LCD_MOSI            11
#define PAD_PIN_LCD_MISO            -1
#define PAD_PIN_LCD_CS              17
#define PAD_PIN_LCD_RST             3   // Or set to -1 and connect to Arduino RESET pin
#define PAD_PIN_LCD_DC              18

#define ENCODER_A           48
#define ENCODER_B           45
#define ENCODER_SW          47

#define PAD_ALT_LEDS        21
#define PAD_KEY_LEDS        6

#define KEY_TAP_THRESHOLD_US 300000
#define KEY_HELD_THRESHOLD_US 600000
#define KEY_COUNT 9

uint16_t key_masks[KEY_COUNT] = {
  0x0008, 0x0004, 0x0002,
  0x0080, 0x0040, 0x0020,
  0x0100, 0x0200, 0x0400
};

uint8_t keyboard_master[6] = {0x7c, 0xdf, 0xa1, 0xe0, 0x70, 0x40};
uint8_t keyboard_slave[6]  = {0x7c, 0xdf, 0xa1, 0xe0, 0x6e, 0xec};

int64_t key_state_updated[KEY_COUNT] = {0};

uint16_t keystate_remote = 0;

bool keys_changed = false;

uint8_t device_index; // 0 = master, 1 = slave
uint8_t remote_index;

static LCD lcd;


/* key definition */




/**
 * @brief i2c master initialization
 */
static esp_err_t i2c_master_init(void) {
  i2c_port_t i2c_master_port = (i2c_port_t)I2C_MASTER_NUM;

  i2c_config_t conf = {
    .mode = I2C_MODE_MASTER,
    .sda_io_num = PAD_PIN_I2C_MASTER_SDA,
    .scl_io_num = PAD_PIN_I2C_MASTER_SCL,
    .sda_pullup_en = GPIO_PULLUP_ENABLE,
    .scl_pullup_en = GPIO_PULLUP_ENABLE,
    .master = {
      .clk_speed = I2C_MASTER_FREQ_HZ
    },
    .clk_flags = 0
  };

  i2c_param_config(i2c_master_port, &conf);

  return i2c_driver_install(i2c_master_port, conf.mode, I2C_MASTER_RX_BUF_DISABLE, I2C_MASTER_TX_BUF_DISABLE, 0);
}

/**
 * @brief read bytes from TCA9555 register
 */
static esp_err_t tca9555_register_read(uint8_t reg_addr, uint8_t *data, size_t len) {
  return i2c_master_write_read_device((i2c_port_t)I2C_MASTER_NUM, TCA9555_SENSOR_ADDR, &reg_addr, 1, data, len, I2C_MASTER_TIMEOUT_MS / portTICK_PERIOD_MS);
}

static uint16_t read_key_state(void) {
  uint8_t data;
  uint16_t key_state = 0;

  key_state = 0;
  ESP_ERROR_CHECK_WITHOUT_ABORT(tca9555_register_read(TCA9555_INPUT_PORT_REGISTER_0, &data, 1));
  key_state |= (data);
  ESP_ERROR_CHECK_WITHOUT_ABORT(tca9555_register_read(TCA9555_INPUT_PORT_REGISTER_1, &data, 1));
  key_state |= (data << 8);

  return key_state;
}

static void IRAM_ATTR gpio_isr_handler(void *arg) {
  keys_changed = true;
}

void process_keys(uint16_t last_state, uint16_t state, int64_t timestamp) {
  for (uint i=0; i<KEY_COUNT; i++) {
    if (key_masks[i] & state) {
      // key is up

      if (key_masks[i] & last_state) {
        // unchanged
      } else if (timestamp - key_state_updated[i] < KEY_TAP_THRESHOLD_US) {
        // held
        printf("TAPPED: %02X\n", state);
      } else {
        // released
        key_state_updated[i] = timestamp;

        printf("RELEASED: %02X\n", state);
      }
    } else {
      // key is down
      if (key_masks[i] & last_state) {
        // pressed
        key_state_updated[i] = timestamp;

        printf("PRESSED: %02X\n", state);

        // send to queue
      } else if (timestamp - key_state_updated[i] > KEY_HELD_THRESHOLD_US) {
        // held

        printf("HELD: %02X\n", state);
      } else {
        // unchanged
      }
    }
  }
}

void ui_render_blink(bool state) {
 uint8_t x = 4;
 uint8_t y = 4;

 if (state) {
    lcd.fillRect(x, y, 8, 8, TFT_WHITE);
  } else {
    lcd.fillRect(x, y, 8, 8, TFT_RED);
  }
}

void ui_render_encoder(uint8_t index, uint8_t device_count, int16_t state) {
  uint8_t x = 80 - (device_count << 5) + (index << 6);
  uint8_t y = 16 + 48;

  lcd.drawRect(x, y, 48, 8, TFT_WHITE);
  if (state >= 0 && state <= 46) {
    lcd.fillRect(x+1,         y+1, state,    6, TFT_WHITE);
    lcd.fillRect(x+1 + state, y+1, 46-state, 6, TFT_BLACK);
  }
}

void ui_render_keys(uint8_t index, uint8_t device_count, uint16_t state) {
  uint8_t xs = 80 - (device_count * 64 / 2) + (index * 64);
  uint8_t xy = 10;
  uint8_t x, y;

  for (uint i=0; i<KEY_COUNT; i++) {
    x = xs + (i%3) * 16;
    y = xy + (i/3) * 16;

    if (key_masks[i] & state) {
      lcd.fillRect(x, y, 15, 15, TFT_WHITE);
    } else {
      lcd.fillRect(x, y, 15, 15, TFT_BLUE);
    }
  }
}

#ifdef __cplusplus
extern "C" {
#endif

void app_main(void) {
  uint8_t base_mac_addr[6] = {0};
  esp_err_t ret = ESP_OK;
  ret = esp_efuse_mac_get_default(base_mac_addr);
  if (ret != ESP_OK) {
      ESP_LOGE(TAG, "Failed to get base MAC address from EFUSE BLK0. (%s)", esp_err_to_name(ret));
      ESP_LOGE(TAG, "Aborting");
      abort();
  } else {
      ESP_LOGI(TAG, "Base MAC Address read from EFUSE BLK0");
  }

  esp_storage_init();

  // set the base mac address using the retrieved mac address
  ESP_LOGI(TAG, "Using \"0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x\" as base MAC address",
           base_mac_addr[0], base_mac_addr[1], base_mac_addr[2], base_mac_addr[3], base_mac_addr[4], base_mac_addr[5]);

  // determine device index
  if(memcmp(base_mac_addr, keyboard_master, 6) == 0) {
    printf("DEVICE: MASTER\n");
    device_index = 0;
    remote_index = 1;
  } else if(memcmp(base_mac_addr, keyboard_slave, 6) == 0) {
    printf("DEVICE: SLAVE\n");
    device_index = 1;
    remote_index = 0;
  } else {
    printf("DEVICE: UNKNOWN\n");
  }

  emk_wifi_init();

  ret = emk_espnow_init(device_index);
  if (ret != ESP_OK) {
      ESP_LOGE(TAG, "Failed to initialise ESP NOW subsystem BLK0. (%s)", esp_err_to_name(ret));
      abort();
  }

  // register keyboard queue
  ESP_ERROR_CHECK(i2c_master_init());
  ESP_LOGI(TAG, "I2C initialised successfully");

  /* initialise ISR */
  gpio_config_t io_conf = {};

  io_conf.intr_type = GPIO_INTR_POSEDGE;
  io_conf.pin_bit_mask = (1ULL << PAD_PIN_IO_EXPANDER_INT);
  io_conf.mode = GPIO_MODE_INPUT;
  //io_conf.pull_up_en = 1;
  gpio_config(&io_conf);

  //install gpio isr service
  gpio_install_isr_service(0);

  //hook isr handler for specific gpio pin
  gpio_isr_handler_add((gpio_num_t)PAD_PIN_IO_EXPANDER_INT, gpio_isr_handler, NULL);

  printf("Minimum free heap size: %ld bytes\n", esp_get_minimum_free_heap_size());

  lcd.init();

  ui_render_blink(false);
  ui_render_keys(0, 2, 0xffff);
  ui_render_keys(1, 2, 0xffff);
  ui_render_encoder(0, 2, 0);

  int cnt = 0;
  int64_t last_update = 0;
  uint16_t last_key_state = read_key_state();
  uint16_t key_state = 0;
  bool blink_state = false;

  uint16_t last_keystate_remote = 0;

  keystate_t ks;

  while(1) {
    int64_t now = esp_timer_get_time();

    /* process keys - start */
    key_state = read_key_state();
    process_keys(last_key_state, key_state, now);

    if (key_state != last_key_state) {
      ks.key_state = (uint32_t)key_state;
      ks.time_ref = now;

      emk_espnow_send_keystate(ks);

      ui_render_keys(device_index, 2, key_state);
      last_key_state = key_state;
    }

    if (last_keystate_remote != keystate_remote) {
      ui_render_keys(remote_index, 2, keystate_remote);
      last_keystate_remote = keystate_remote;
    }

    /* process keys - end */

    if (now - last_update > 500000) {
      printf("CNT: %d\n", cnt++);

      blink_state = !blink_state;
      ui_render_blink(blink_state);

      last_update = now;
    }

    vTaskDelay(10 / portTICK_PERIOD_MS);
  }

  printf("Restarting now.\n");
  fflush(stdout);
  esp_restart();
}

#ifdef __cplusplus
}
#endif

