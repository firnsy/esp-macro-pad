#if ARDUINO_USB_MODE
#error This sketch must be used when USB is in OTG mode
#endif /* ARDUINO_USB_MODE */

#include "USB.h"
#include "USBHIDKeyboard.h"
USBHIDKeyboard Keyboard;

#include <TFT_eSPI.h>
#include <ESP32Encoder.h>

#include <SPI.h>
#include "TCA9555.h"
#include <Wire.h>

ESP32Encoder encoder;

TFT_eSPI tft = TFT_eSPI();
TCA9555 TCA = TCA9555(0x20);


#define PAD_I2C_SDA         35
#define PAD_I2C_SCL         0
#define PAD_I2C_CLOCK_SPEED 400000UL

#define PAD_KEY_INT         36

#define TFT_CS              17
#define TFT_RST             3   // Or set to -1 and connect to Arduino RESET pin
#define TFT_DC              18

#define ENCODER_A           48
#define ENCODER_B           45
#define ENCODER_SW          47

#define PAD_ALT_LEDS        21
#define PAD_KEY_LEDS        6

#define KEY_COUNT 9
uint16_t keyMasks[KEY_COUNT] = {
  0x008,0x004,0x002,
  0x080,0x040,0x020,
  0x100,0x200,0x400   
};

volatile byte keyChange = false;

uint16_t keyState = 0;
uint16_t lastKeyState = 0;

bool blinkState = false;
uint32_t lastBlink = 0;

int16_t lastEncoderState = 0;


void IRAM_ATTR keyChangeISR() {
  keyChange = true;
}

void setup()
{
  Serial.begin(115200);
  
  pinMode(PAD_KEY_INT, INPUT);
  attachInterrupt(digitalPinToInterrupt(PAD_KEY_INT), keyChangeISR, FALLING);
  
  Wire.begin();
  //Wire.setClock(PAD_I2C_CLOCK_SPEED);
  TCA.begin();
  
  tft.init();
  tft.setRotation(3);
  tft.fillScreen(TFT_BLACK);
  tft.setTextSize(1);

  lastKeyState = keyState = TCA.read16();
  uiRenderKeys(keyState);

  encoder.attachSingleEdge(ENCODER_A, ENCODER_B);
  lastEncoderState = encoder.getCount();
  Serial.printf("Encoder Start: %d\n", lastEncoderState);
  uiRenderEncoder(lastEncoderState);

  Keyboard.begin();
  USB.begin();

  Serial.printf("Initialised.\n");
}


void loop() {
  uint32_t n = millis();

  // read keys
/*
  if (keyChange) {
  */
    keyState = TCA.read16();
  
    if (keyState != lastKeyState) {
      uint16_t keyStateChanged = keyState ^ lastKeyState;
  
      uiRenderKeys(keyState);

      applyKeys(keyState);
      
      lastKeyState = keyState;
    }

  // read encoder
  int16_t encoderState = encoder.getCount();

  if (encoderState != lastEncoderState) {
    uiRenderEncoder(encoderState);

    lastEncoderState = encoderState;
  }

/*
    keyChange = false;
  }
  */

  if (n - lastBlink > 500) {
    blinkState = !blinkState;

    uiRenderBlink(blinkState);
    
    lastBlink = n;
  }
}

void applyKeys(uint16_t state) {
  for (uint i=0; i<KEY_COUNT; i++) {
    if (!(keyMasks[i] & state)) {
      Keyboard.press((char)(65+i));
    } else {
      Keyboard.release((char)(65+i));
    }
  }
}

void uiRenderBlink(bool state) {
 uint8_t x = 4;
 uint8_t y = 4;
  
 if (state) {
    tft.fillRect(x, y, 8, 8, TFT_WHITE);
  } else {
    tft.fillRect(x, y, 8, 8, TFT_RED);
  }
}

void uiRenderKeys(uint16_t state) {
  uint8_t x, y;
  
  for (uint i=0; i<KEY_COUNT; i++) {
    x = 96 + (i%3) * 16;
    y = 16 + (i/3) * 16;
    
    if (keyMasks[i] & state) {
      tft.fillRect(x, y, 12, 12, TFT_WHITE);
    } else {
      tft.fillRect(x, y, 12, 12, TFT_BLUE);
    }
  }
}

void uiRenderEncoder(int16_t state) {
  uint8_t x = 16;
  uint8_t y = 36;

  tft.drawRect(x, y, 52, 8, TFT_WHITE);
  if (state >= 0 && state <= 50) {
    tft.fillRect(x+1,         y+1, state,    6, TFT_WHITE);
    tft.fillRect(x+1 + state, y+1, 50-state, 6, TFT_BLACK);
  }
}
